# studies: Reference study

[![pipeline status](https://gitlab.inria.fr/tutorial-guix-hpc-workshop/studies/reference/badges/main/pipeline.svg)](https://gitlab.inria.fr/tutorial-guix-hpc-workshop/studies/reference/-/commits/main)

This repository contains a copy of a minimal working example of an experimental
study featuring the `minisolver` application [3] and relies on the GNU Guix [1]
transactional package manager and to ensure reproducibility of the research
study. We refer to this version of the study as to the *Reference* study.

[Manuscript](https://tutorial-guix-hpc-workshop.gitlabpages.inria.fr/studies/reference/study.pdf).

## Reproducing guidelines

In order to reproduce the software environment using Guix for performing
experiments, we rely on a list of channels and a dedicated manifest file (see
[1] for further details) and the combination of the `guix time-machine` and the
`guix shell` commands. This is followed by the command for launching the
experiments which will be executed in that environment. 

`minisolver`'s option `--batch-input` allows us to perform a batch of benchmarks
defined in a comma-separated values file.

The batch file listing all the benchmarks to run has a very simple format. The
first column holds the size of the linear system to solve and the second column
specifies the compression level to use.

```bash
guix time-machine -C channels.scm -- shell --pure -m manifest.scm -- \
     minisolver --batch-input benchmarks.csv --batch-output results.csv
```

Then, to post-process the results and produce the figures featured in the
manuscript, we rely on a script written in the R language and using the
`ggplot2` graphics library, i.e. the [plot.R](plot.R) file. The script can be
executed the following way.

```bash
guix time-machine -C channels.scm -- shell --pure -m manifest.scm -- \
     Rscript plot.R
```

Finally, we use LaTeX to produce the manuscript in PDF.

```bash
guix time-machine -C channels.scm -- shell --pure -m manifest.scm -- \
     latexmk --shell-escape -f -pdf -pdflatex -interaction=nonstopmode study.tex
```

## References

1. GNU Guix software distribution and transactional package manager
   [https://guix.gnu.org](https://guix.gnu.org).
2. Literate Programming, Donald E. Knuth, 1984
   [https://doi.org/10.1093/comjnl/27.2.97](https://doi.org/10.1093/comjnl/27.2.97).
3. minisolver, a simple application for testing dense solvers with pseudo-BEM
   matrices
   [https://gitlab.inria.fr/tutorial-guix-hpc-workshop/software/minisolver](https://gitlab.inria.fr/tutorial-guix-hpc-workshop/software/minisolver).
