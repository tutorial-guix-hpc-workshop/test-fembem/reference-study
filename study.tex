\documentclass[a4paper, 11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{Marek Felšöci}
\date{November 08, 2023}
\title{Minimal working example of an experimental study written in Org mode}
\hypersetup{
 pdfauthor={Marek Felšöci},
 pdftitle={Minimal working example of an experimental study written in Org mode},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 28.2 (Org mode 9.6.7)}, 
 pdflang={English}}
\usepackage{biblatex}
\addbibresource{/home/marek/src/tutorial-guix-hpc-workshop/studies/org-modized/references.bib}
\begin{document}

\maketitle

\section{Introduction}
\label{sec:org907e856}
This is a minimal, yet complete enough, working example of an experimental study
written in Org mode and relying on GNU Guix for the management of its software
environment. From the scientific point of view, we are interested in solving
linear systems similar to those arising from discrete models in the domain of
aeroacoustic numerical simulations. In this context, we analyze the performance
of a simple numerical solver of such kind of linear systems, the \texttt{minisolver}
application, developed especially for the needs of this study.

\section{Experimental model and software}
\label{sec:orgbb3ed53}
\texttt{minisolver} \autocite{minisolver} considers a simplified aeroacoustic \texttt{cylinder}
model represented by a linear system. It is then able to generate test cases of
arbitrarily large sizes in terms of the unknown count in the corresponding
linear system. Finally, \texttt{minisolve} uses the open-source HMAT solver
\autocite{hmat-oss,Lize14} for solving the generated linear system and actually
compute the acoustic pressure in different parts of the model.

\begin{figure}[htbp]
\centering
\includegraphics[width=.5\columnwidth]{./figures/cylinder.png}
\caption{\label{fig:orge030638}A \texttt{cylinder} mesh counting 20,000 vertices.}
\end{figure}

It is to note that the HMAT solver implements a data compression method which
allows the solver to reduce not only the amount of computations but also the
amount of memory it has to perform in order to solve a linear system. The solver
can be instrumented to use different levels of compression, i.e. \emph{low}, \emph{medium}
or \emph{high}, depending on the desired solution accuracy. The compression can also
be \emph{disabled}.

\section{Numerical study}
\label{sec:org5b4faa6}
The goal of the study is to perform a simple performance evaluation of
\texttt{minisolver} on linear systems of different size and using different levels of
compression.

\subsection{Experimental setup}
\label{sec:orgc3f4a7c}
\subsubsection{Hardware environment}
\label{sec:orge3a3fa8}
All the benchmarks were conducted on a single 12th generation Intel(R) Core(TM)
i5-12500H processor in sequential mode, i.e. using one thread, and with 16 GB of
available RAM.

\subsubsection{Software environment}
\label{sec:orga585769}
We rely on the version \texttt{0.1-1.b5c4098} of \texttt{minisolver} and the version
\texttt{1.7.1-32.6a4e21b} of \texttt{hmat-oss}. These packages were compiled with GNU C
Compiler 13.2.0 and the OpenBLAS library 0.3.2.

\subsection{Benchmarks}
\label{sec:orgb2db798}
We benchmark \texttt{minisolver} on linear systems with \(N\), the number of unknowns,
in (5,000; 10,000; 15,000; 20,000) while consider either the \emph{low} or the \emph{high}
compression level. Table \ref{tab:org8223205} lists all of the considered
benchmarks.

\begin{table}[htbp]
\caption{\label{tab:org8223205}List of benchmarks.}
\centering
\begin{tabular}{cc}
Linear system size & Compression level\\[0pt]
\hline
5000 & high\\[0pt]
5000 & low\\[0pt]
10000 & high\\[0pt]
10000 & low\\[0pt]
15000 & high\\[0pt]
15000 & low\\[0pt]
20000 & high\\[0pt]
20000 & low\\[0pt]
\end{tabular}
\end{table}

\subsubsection{Computation time}
\label{sec:orgc19ac7d}
In the first part of the results analysis, we want to show to which extent can
data compression improve the computation time. For this, we compare sequential
executions of \texttt{minisolver} using two different compression levels, i.e. \texttt{high}
and \texttt{low}, on linear systems of different sizes (see Figure \ref{fig:orgedd9e91}).
The results clearly show the advantage of using data compression, especially
with increasing size of the target linear system.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\columnwidth]{./figures/results-time.pdf}
\caption{\label{fig:orgedd9e91}Computation times of sequential runs of \texttt{minisolver} on linear systems of varying size and using different compression levels.}
\end{figure}

\subsubsection{Peak RAM usage}
\label{sec:org788e28e}
In the second part, we want to know to which extent can data compression improve
the RAM usage of the solver. For this, we compare peak RAM usages of sequential
executions of HMAT using two different compression levels, i.e. \texttt{high} and
\texttt{low}, on linear systems of different sizes (see Figure \ref{fig:orgbfeb554}). Again,
the results clearly show the advantage of using data compression, especially
with increasing size of the target linear system.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\columnwidth]{./figures/results-ram.pdf}
\caption{\label{fig:orgbfeb554}RAM usage peaks of sequential runs of \texttt{minisolver} on linear systems of varying size and using different compression levels.}
\end{figure}

\section{Conclusion}
\label{sec:orge7c4ba9}
We have evaluated the performance of the \texttt{minisolver} application on linear
systems arising from a simplified aerocoustic model. The comparison of
sequential runs of \texttt{minisolver} showed an important positive impact of data
compression on the time to solution as well as on the RAM usage.

\section{References}
\label{sec:orga8c3b21}
\printbibliography
\end{document}
